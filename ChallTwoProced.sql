drop procedure if exists sqlQ2;

DELIMITER //
create procedure sqlQ2 ()

BEGIN

create temporary table biggest_price_range
select DATE_FORMAT(`Date`, '%m/%d/%Y') as TradeDate,`OpenPrice`, `ClosePrice`, abs(`OpenPrice` - `ClosePrice`) as price_range
from trade_data_c2
order by price_range DESC
limit 4;

create temporary table max_high_price
select DATE_FORMAT(`Date`, '%m/%d/%Y') as TradeDate, max(HighPrice) as MaxPrice
from trade_data_c2
where DATE_FORMAT(`Date`, '%m/%d/%Y') in ( select distinct `TradeDate` from biggest_price_range)
group by DATE_FORMAT(`Date`, '%m/%d/%Y');


create temporary table time_at_max_price
select DATE_FORMAT(`Date`, '%m/%d/%Y') as `Date`, `Time`, `HighPrice`
from trade_data_c2
inner join max_high_price on `TradeDate` = DATE_FORMAT(`Date`, '%m/%d/%Y') and `MaxPrice` = `HighPrice`;


create temporary table date_range_time
select distinct time_at_max_price.`Date` as `Date`, time_at_max_price.`Time`, biggest_price_range.`price_range`
from biggest_price_range
inner join time_at_max_price on biggest_price_range.`TradeDate` = time_at_max_price.`Date`;

select * from date_range_time;

drop temporary table if exists biggest_price_range;
drop temporary table if exists max_high_price;
drop temporary table if exists time_at_max_price;
drop temporary table if exists date_range_time;


END //

DELIMITER ;

