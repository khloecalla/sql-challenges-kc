#SQL Challenges KC

SQL Challenge Question 1
- The first step was to import table data from the import wizard using 
  the dataset.csv file downloaded from wiki.conygre.com 
- When importing data, I changed the tradeDate from a bigint field type to a date time 
  and changed the date format to %Y%m%d%H%M
- Before importing the data i changed the column names to make it more readable 
  and understandable
- The procedure script is contained in ChallOneProced.sql
- The call script is contained in ChallOneCall.seq

SQL Challenge Question 2
- The first step for chellenge two was to import table data from the import wizard. 
  I changed the field type for date from bigint to date/time. I also changed the date 
  format to %m/%d/%Y
- For challenge two there was several different solutions/outcomes however i choose to 
  do the challenge a particular way due to time constraints.
- The procedure script is congtained in ChallTwoProced.sql
- The call script is contined in ChallTwoCall.sql

