DELIMITER //

CREATE PROCEDURE sqlQ1( IN inputDate datetime, IN tickerNum varchar(10))

BEGIN

SELECT tickerNum,  DATE_FORMAT(tradeDate, '%d/%m/%Y') as TradeDate,
DATE_FORMAT(tradeDate, '%H:%i') as StartTime, DATE_FORMAT(date_add(inputDate,interval 5 hour), '%H:%i') as endTime,
SUM(volume * closePrice) / SUM(volume) AS Volume_Weighted_Price
from trade_data
WHERE tradeDate between inputDate AND DATE_ADD(inputDate,interval 5 hour) 

AND tickerNum = tickerNum
GROUP BY tickerNum;



END //







/*

SELECT TRADING_SYMBOL,
SUM(TRADE_SIZE*TRADE_PRICE)/SUM(TRADE_SIZE) as VOLUME_WEIGHTED_PRICE
FROM STOCK_TRADE
WHERE TRADE_TIME BETWEEN '2005-11-14 12:00' AND '2005-11-14 15:00'
AND TRADING_SYMBOL = 'ADV'
GROUP BY TRADING_SYMBOL


*/